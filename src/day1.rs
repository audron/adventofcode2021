#[aoc_generator(day1)]
pub fn input_generator(input: &str) -> Vec<u32> {
    input.lines().map(|x| x.parse().unwrap()).collect()
}

#[aoc(day1, part1)]
pub fn solve_part1(input: &[u32]) -> u32 {
    input.iter().fold((0, 0), |(prev, count), x| {
        if *x > prev && prev != 0 {
            return (*x, count + 1)
        }

        return (*x, count)
    }).1
}

#[aoc(day1, part2)]
pub fn solve_part2(input: &[u32]) -> u32 {
    input.windows(3).fold((0, 0), |(prev, count), x| {
        let sum: u32 = x.iter().sum();

        if sum > prev && prev != 0 {
            return (sum, count + 1)
        }

        return (sum, count);
    }).1
}

#[test]
fn test_part1() {
    let input: Vec<u32> = vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263];

    assert_eq!(7, solve_part1(&input))
}

#[test]
fn test_part2() {
    let input: Vec<u32> = vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263];

    assert_eq!(5, solve_part2(&input))
}
