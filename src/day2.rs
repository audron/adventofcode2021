#[derive(Clone, Debug)]
pub enum Direction {
    Forward(i32),
    Down(i32),
    Up(i32),
}

#[aoc_generator(day2)]
pub fn input_generator(input: &str) -> Vec<Direction> {
    input
        .lines()
        .map(|line| {
            let mut line = line.split_ascii_whitespace();
            let direction = line.next().unwrap();
            let amount = line.next().unwrap().parse().unwrap();

            match direction {
                "forward" => Direction::Forward(amount),
                "down" => Direction::Down(amount),
                "up" => Direction::Up(amount),
                _ => unimplemented!(),
            }
        })
        .collect()
}

#[aoc(day2, part1)]
pub fn solve_part1(input: &[Direction]) -> i32 {
    let res = input.iter().fold((0, 0), |(x, y), dir| {
        match dir {
            Direction::Forward(i) => (x + i, y),
            Direction::Down(i) => (x, y + i),
            Direction::Up(i) => (x, y - i),
        }
    });

    res.0 * res.1
}

#[aoc(day2, part2)]
pub fn solve_part2(input: &[Direction]) -> i32 {
    let res = input.iter().fold((0, 0, 0), |(x, y, aim), dir| {
        match dir {
            Direction::Forward(i) => (x + i, y + (aim * i), aim),
            Direction::Down(i) => (x, y, aim + i),
            Direction::Up(i) => (x, y, aim - i),
        }
    });

    res.0 * res.1
}

#[allow(dead_code)]
static TEST_INPUT: &str = r#"forward 5
down 5
forward 8
up 3
down 8
forward 2
"#;

#[test]
pub fn test_part1() {
    let input = input_generator(TEST_INPUT);

    assert_eq!(150, solve_part1(&input));
}

#[test]
pub fn test_part2() {
    let input = input_generator(TEST_INPUT);

    assert_eq!(900, solve_part2(&input));
}
