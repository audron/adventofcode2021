#[aoc_generator(day3)]
pub fn input_generator(input: &str) -> Vec<u32> {
    input
        .lines()
        .map(|line| u32::from_str_radix(line, 2).unwrap())
        .collect()
}

pub fn solve_part1_internal(input: &[u32], bit_length: i32) -> u32 {
    let mut res = 0;

    for position in 0..31 {
        if most_common(input, position) == MostCommon::One {
            res |= 1 << position;
        }
    }

    println!("{:#b} * {:#b}", res, !res ^ (u32::MAX << bit_length));

    res * (!res ^ (u32::MAX << bit_length))
}

#[aoc(day3, part1)]
pub fn solve_part1(input: &[u32]) -> u32 {
    solve_part1_internal(input, 12)
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum MostCommon {
    One,
    Tie,
    Zero,
}

// count the number of 1 in a position in bytes
// if it there are more 1 than 0 return true
pub fn most_common(input: &[u32], position: u32) -> MostCommon {
    let count = input.iter().fold(0, |count, x| {
        if (x & (1 << position)) == (1 << position) {
            return count + 1;
        } else {
            return count;
        }
    });

    if count as f64 > input.len() as f64 / 2.0 {
        MostCommon::One
    } else if (count as f64) < input.len() as f64 / 2.0 {
        MostCommon::Zero
    } else {
        MostCommon::Tie
    }
}

pub fn get_oxygen(input: &[u32], mut position: u32) -> u32 {
    match input {
        [] => 0,
        [i] => *i,
        i => {
            position -= 1;

            let i: Vec<u32> = match most_common(input, position) {
                MostCommon::Tie | MostCommon::One => i
                    .iter()
                    .copied()
                    .filter(|i| *i & (1 << position) == (1 << position))
                    .collect(),
                MostCommon::Zero => i
                    .iter()
                    .copied()
                    .filter(|i| *i & (1 << position) != (1 << position))
                    .collect(),
            };

            get_oxygen(&i, position)
        }
    }
}

pub fn get_co2(input: &[u32], mut position: u32) -> u32 {
    match input {
        [] => 0,
        [i] => *i,
        i => {
            position -= 1;

            let i: Vec<u32> = match most_common(input, position) {
                MostCommon::Tie | MostCommon::One => i
                    .iter()
                    .copied()
                    .filter(|i| *i & (1 << position) != (1 << position))
                    .collect(),
                MostCommon::Zero => i
                    .iter()
                    .copied()
                    .filter(|i| *i & (1 << position) == (1 << position))
                    .collect(),
            };

            get_co2(&i, position)
        }
    }
}

pub fn solve_part2_internal(input: &[u32], bit_length: u32) -> u32 {
    let oxygen = get_oxygen(input, bit_length);
    let co2 = get_co2(input, bit_length);

    oxygen * co2
}

#[aoc(day3, part2)]
pub fn solve_part2(input: &[u32]) -> u32 {
    solve_part2_internal(input, 12)
}

#[allow(dead_code)]
pub static TEST_INPUT: &str = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010
";

#[test]
fn test_part1() {
    assert_eq!(198, solve_part1_internal(&input_generator(TEST_INPUT), 5))
}

#[test]
fn test_part2() {
    assert_eq!(230, solve_part2_internal(&input_generator(TEST_INPUT), 5))
}
