use std::fmt::{Display, Write};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct BingoBoard(Vec<Vec<BingoNumber>>, bool);

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct BingoNumber {
    marked: bool,
    number: u32,
}

impl From<u32> for BingoNumber {
    fn from(i: u32) -> Self {
        Self {
            marked: false,
            number: i,
        }
    }
}

impl Display for BingoBoard {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.iter().for_each(|row| {
            row.iter()
                .for_each(|i| f.write_fmt(format_args!("{}, ", i.number)).unwrap());
            f.write_char('\n').unwrap();
        });

        Ok(())
    }
}

impl BingoBoard {
    pub fn parse(input: &str) -> BingoBoard {
        BingoBoard(
            input
                .lines()
                .map(|line| {
                    line.split_ascii_whitespace()
                        .map(|i| i.parse::<u32>().unwrap().into())
                        .collect()
                })
                .collect(),
            false,
        )
    }

    pub fn mark(&mut self, num: u32) -> bool {
        self.apply_marks(num);
        if self.check_win() {
            self.1 = true;
            return true;
        } else {
            return false;
        }
    }

    pub fn apply_marks(&mut self, num: u32) {
        self.0.iter_mut().for_each(|row| {
            row.iter_mut()
                .filter(|i| i.number == num)
                .for_each(|i| i.marked = true);
        });
    }

    pub fn check_win(&self) -> bool {
        let mut marked = false;

        self.0.iter().for_each(|row| {
            marked |= row.iter().all(|i| i.marked == true)
        });

        let len = self.0[0].len();

        for pos in 0..(len - 1) {
            marked |= self.0.iter().all(|row| row[pos].marked == true);
        }

        return marked;
    }

    pub fn sum_of_unmarked(&self) -> u32 {
        self.0.iter().fold(0, |sum, row| {
            sum + row
                .iter()
                .filter(|i| i.marked == false)
                .map(|i| i.number)
                .sum::<u32>()
        })
    }
}

#[aoc_generator(day4)]
pub fn input_generator(input: &str) -> (Vec<u32>, Vec<BingoBoard>) {
    let mut boards = input.split("\n\n");

    let order = boards
        .next()
        .unwrap()
        .split(',')
        .map(|i| i.parse().unwrap())
        .collect();

    let boards = boards.map(|board| BingoBoard::parse(board)).collect();

    (order, boards)
}

#[aoc(day4, part1)]
pub fn solve_part1((order, boards): &(Vec<u32>, Vec<BingoBoard>)) -> u32 {
    let mut boards = boards.clone();

    for num in order {
        for board in boards.iter_mut() {
            if board.mark(*num) {
                return num
                    * board.0.iter().fold(0, |sum, row| {
                        sum + row
                            .iter()
                            .filter(|i| i.marked == false)
                            .map(|i| i.number)
                            .sum::<u32>()
                    });
            }
        }
    }

    0
}

#[aoc(day4, part2)]
// 17388
pub fn solve_part2((order, boards): &(Vec<u32>, Vec<BingoBoard>)) -> u32 {
    let mut boards = boards.clone();

    let board_len = boards.len();
    let mut count = 0;

    for num in order {
        for board in boards.iter_mut() {
            board.apply_marks(*num)
        }

        while let Some(winner) = boards.iter_mut().filter(|b| !b.1 && b.check_win()).next() {
            winner.1 = true;
            count += 1;
            println!("{}", winner.sum_of_unmarked() * num);
            if count == board_len {
                return winner.sum_of_unmarked() * num;
            }
        }
    }

    0
}

#[cfg(test)]
static TEST_INPUT: &str = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7
";

#[test]
fn test_order_parse() {
    assert_eq!(
        vec![
            7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24, 10, 16, 13, 6, 15, 25, 12, 22, 18, 20, 8, 19,
            3, 26, 1
        ],
        input_generator(TEST_INPUT).0
    )
}

#[test]
fn test_bingo_board_parse() {
    assert_eq!(
        BingoBoard(
            vec![
                vec![22.into(), 13.into(), 17.into(), 11.into(), 0.into()],
                vec![8.into(), 2.into(), 23.into(), 4.into(), 24.into()],
                vec![21.into(), 9.into(), 14.into(), 16.into(), 7.into()],
                vec![6.into(), 10.into(), 3.into(), 18.into(), 5.into()],
                vec![1.into(), 12.into(), 20.into(), 15.into(), 19.into()],
            ],
            false
        ),
        BingoBoard::parse(
            "22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19
"
        )
    )
}

#[test]
fn test_bingo_board_mark() {
    let mut board = BingoBoard::parse(
        "22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19
",
    );

    assert_eq!(false, board.mark(22));
    assert_eq!(false, board.mark(13));
    assert_eq!(false, board.mark(17));
    assert_eq!(false, board.mark(11));
    assert_eq!(true, board.mark(0));
}

#[test]
fn test_part1() {
    let input = input_generator(TEST_INPUT);

    assert_eq!(4512, solve_part1(&input))
}

#[test]
fn test_part2() {
    let input = input_generator(TEST_INPUT);

    assert_eq!(1924, solve_part2(&input))
}
