use std::{
    cmp::Ordering,
    collections::HashMap,
    fmt::{Display, Write},
    num::ParseIntError,
    ops::{Deref, DerefMut},
    str::FromStr,
};

use itertools::Itertools;

#[derive(Debug, Clone, PartialEq, Eq, Ord, Hash)]
pub struct Point {
    x: i32,
    y: i32,
}

impl PartialOrd for Point {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        let y = self.y.cmp(&other.y);
        let x = self.x.cmp(&other.x);

        let ordering = match y {
            Ordering::Less => y,
            Ordering::Equal => x,
            Ordering::Greater => y,
        };

        Some(ordering)
    }
}

impl FromStr for Point {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut s = s.split(',');

        Ok(Point {
            x: s.next().unwrap().parse()?,
            y: s.next().unwrap().parse()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Line(Point, Point);

impl FromStr for Line {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut s = s.split(" -> ");

        Ok(Line(s.next().unwrap().parse()?, s.next().unwrap().parse()?))
    }
}

impl Line {
    pub fn direction(&self) -> Direction {
        let Line(a, b) = self;

        if a.y == b.y {
            if a.x > b.x {
                Direction::West
            } else {
                Direction::East
            }
        } else if a.x == b.x {
            if a.y > b.y {
                Direction::North
            } else {
                Direction::South
            }
        } else {
            let mut x = [a.x, b.x];
            x.sort();

            let mut y = [a.y, b.y];
            y.sort();

            if x[1] - x[0] == y[1] - y[0] {
                if a.y > b.y {
                    if a.x > b.x {
                        Direction::NorthWest
                    } else {
                        Direction::NorthEast
                    }
                } else if a.y < b.y {
                    if a.x > b.x {
                        Direction::SouthWest
                    } else {
                        Direction::SouthEast
                    }
                } else {
                    Direction::None
                }
            } else {
                Direction::None
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Direction {
    East,
    West,
    North,
    South,
    NorthEast,
    NorthWest,
    SouthEast,
    SouthWest,
    None,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Grid(HashMap<Point, i32>);

impl Deref for Grid {
    type Target = HashMap<Point, i32>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Grid {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl Display for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        println!("{:#?}", self.0.iter().sorted());

        let x_max = self.0.iter().map(|(Point { x, y }, _)| x).max().unwrap();
        let y_max = self.0.iter().map(|(Point { x, y }, _)| y).max().unwrap();

        let mut x = 0;
        let mut y = 0;

        f.write_char('.')?;

        for (point, i) in self.0.iter().sorted() {
            if point.y > y {
                if x < *x_max + 1 && x != 0 {
                    for _ in x..*x_max {
                        f.write_char('.')?;
                    }
                }

                f.write_char('\n')?;

                if point.y > y + 1 {
                    for _ in y..point.y {
                        f.write_fmt(format_args!("{:.<1$}\n", "", (*x_max + 1) as usize))?;
                    }
                }

                if point.x > 0 {
                    for _ in 0..point.x {
                        f.write_char('.')?;
                    }
                }
            } else {
                if point.x > x + 1 {
                    for _ in (x + 1)..point.x {
                        f.write_char('.')?;
                    }
                }
            }

            y = point.y;

            f.write_str(&i.to_string())?;

            if point.y > y {
                x = 0;
            } else {
                x = point.x;
            }
        }

        for _ in x..*x_max {
            f.write_char('.')?;
        }

        Ok(())
    }
}

fn mark_straight_lines(grid: &mut Grid, lines: &[Line]) {
    lines
        .iter()
        .filter(|Line(a, b)| a.x == b.x)
        .for_each(|Line(a, b)| {
            let mut v = [a.y, b.y];
            v.sort();
            for y in v[0]..(v[1] + 1) {
                *grid.entry(Point { x: a.x, y }).or_default() += 1;
            }
        });

    lines
        .iter()
        .filter(|Line(a, b)| a.y == b.y)
        .for_each(|Line(a, b)| {
            let mut v = [a.x, b.x];
            v.sort();
            for x in v[0]..(v[1] + 1) {
                *grid.entry(Point { x, y: a.y }).or_default() += 1;
            }
        });
}

fn mark_diagonal_lines(grid: &mut Grid, lines: &[Line]) {
    lines
        .iter()
        .filter(|line| match line.direction() {
            Direction::SouthEast
            | Direction::SouthWest
            | Direction::NorthEast
            | Direction::NorthWest => true,
            _ => false,
        })
        .for_each(|line| {
            let mut x = line.0.x;
            let mut y = line.0.y;

            match line.direction() {
                Direction::NorthEast => {
                    while (x - 1, y + 1) != (line.1.x, line.1.y) {
                        *grid.entry(Point { x, y }).or_default() += 1;

                        x += 1;
                        y -= 1;
                    }
                }
                Direction::NorthWest => {
                    while (x + 1, y + 1) != (line.1.x, line.1.y) {
                        *grid.entry(Point { x, y }).or_default() += 1;

                        x -= 1;
                        y -= 1;
                    }
                }
                Direction::SouthEast => {
                    while (x - 1, y - 1) != (line.1.x, line.1.y) {
                        *grid.entry(Point { x, y }).or_default() += 1;

                        x += 1;
                        y += 1;
                    }
                }
                Direction::SouthWest => {
                    while (x + 1, y - 1) != (line.1.x, line.1.y) {
                        *grid.entry(Point { x, y }).or_default() += 1;

                        x -= 1;
                        y += 1;
                    }
                }
                _ => panic!("aaaaa"),
            }
        })
}

#[aoc_generator(day5)]
pub fn input_generator(input: &str) -> Vec<Line> {
    input.lines().map(|line| line.parse().unwrap()).collect()
}

#[aoc(day5, part1)]
pub fn solve_part1(input: &[Line]) -> i32 {
    let mut grid: Grid = Grid(HashMap::new());

    mark_straight_lines(&mut grid, input);

    grid.iter().filter(|(_, i)| **i >= 2).count() as i32
}

#[aoc(day5, part2)]
pub fn solve_part2(input: &[Line]) -> i32 {
    let mut grid: Grid = Grid(HashMap::new());

    mark_straight_lines(&mut grid, input);
    mark_diagonal_lines(&mut grid, input);

    grid.iter().filter(|(_, i)| **i >= 2).count() as i32
}

#[cfg(test)]
static TEST_INPUT: &str = "0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
";

#[test]
fn test_input_generator() {
    assert_eq!(
        Line(Point { x: 0, y: 9 }, Point { x: 5, y: 9 }),
        input_generator(TEST_INPUT)[0]
    )
}

#[test]
fn test_part1() {
    assert_eq!(5, solve_part1(&input_generator(TEST_INPUT)))
}

#[test]
fn test_part2() {
    assert_eq!(12, solve_part2(&input_generator(TEST_INPUT)))
}

#[test]
fn test_partial_cmp_point() {
    assert_eq!(
        Some(Ordering::Greater),
        Point { x: 7, y: 7 }.partial_cmp(&Point { x: 6, y: 7 })
    );

    assert_eq!(
        Some(Ordering::Greater),
        Point { x: 6, y: 7 }.partial_cmp(&Point { x: 7, y: 6 })
    );

    assert_eq!(
        Some(Ordering::Equal),
        Point { x: 7, y: 7 }.partial_cmp(&Point { x: 7, y: 7 })
    );

    assert_eq!(
        Some(Ordering::Less),
        Point { x: 6, y: 7 }.partial_cmp(&Point { x: 7, y: 7 })
    );
}

#[test]
fn test_direction() {
    assert_eq!(
        Direction::SouthEast,
        Line(Point { x: 1, y: 1 }, Point { x: 3, y: 3 }).direction()
    )
}

#[test]
fn test_mark_diag_line() {
    let mut grid: Grid = Grid(HashMap::new());

    mark_diagonal_lines(
        &mut grid,
        &vec![Line(Point { x: 1, y: 1 }, Point { x: 3, y: 3 })],
    );

    assert_eq!(3, grid.iter().filter(|(_, i)| **i >= 1).count());

    let mut grid: Grid = Grid(HashMap::new());

    mark_diagonal_lines(
        &mut grid,
        &vec![Line(Point { x: 3, y: 3 }, Point { x: 1, y: 1 })],
    );

    assert_eq!(3, grid.iter().filter(|(_, i)| **i >= 1).count());

    let mut grid: Grid = Grid(HashMap::new());

    mark_diagonal_lines(
        &mut grid,
        &vec![Line(Point { x: 3, y: 1 }, Point { x: 1, y: 3 })],
    );

    assert_eq!(3, grid.iter().filter(|(_, i)| **i >= 1).count());

    let mut grid: Grid = Grid(HashMap::new());

    mark_diagonal_lines(
        &mut grid,
        &vec![Line(Point { x: 1, y: 3 }, Point { x: 3, y: 1 })],
    );

    assert_eq!(3, grid.iter().filter(|(_, i)| **i >= 1).count());
}
