#[aoc_generator(day6)]
pub fn input_generator(input: &str) -> [u128; 9] {
    // input.split(',').map(|i| i.parse().unwrap()).collect()
    //
    let mut fish = [0; 9];
    input.split(',').for_each(|i| fish[i.parse::<usize>().unwrap()] += 1);

    fish
}

pub fn calculate_growth(mut fish: [u128; 9], days: u32) -> u128 {
    for _ in 0..days {
        let new = fish[0];

        fish.rotate_left(1);

        fish[8] = new;
        fish[6] += new;
    }

    fish.iter().sum()
}

#[aoc(day6, part1)]
pub fn solve_part1(input: &[u128; 9]) -> u128 {
    calculate_growth(*input, 80)
}

#[aoc(day6, part2)]
pub fn solve_part2(input: &[u128; 9]) -> u128 {
    calculate_growth(*input, 256)
}

#[cfg(test)]
static TEST_INPUT: &str = "3,4,3,1,2";

#[test]
fn test_part1()  {
    assert_eq!(26, calculate_growth(input_generator(TEST_INPUT), 18));
    assert_eq!(5934, calculate_growth(input_generator(TEST_INPUT), 80));
}

#[test]
fn test_part2()  {
    assert_eq!(26984457539, calculate_growth(input_generator(TEST_INPUT), 256));
}
