use itertools::Itertools;

#[aoc_generator(day7)]
pub fn input_generator(input: &str) -> Vec<u32> {
    input.split(',').map(|i| i.parse().unwrap()).collect()
}

pub fn median(input: &[u32]) -> f32 {
    let sorted: Vec<&u32> = input.iter().sorted().collect();

    let mid = sorted.len() / 2;

    if sorted.len() % 2 == 0 {
        (sorted[mid - 1] + sorted[mid]) as f32 / 2.0
    } else {
        *sorted[mid] as f32
    }
}

// 529.873µs

#[aoc(day7, part1)]
pub fn solve_part1(input: &[u32]) -> u32 {
    let target = median(input) as u32;

    input.iter().fold(0, |fuel, i| fuel + i.abs_diff(target))
}

#[aoc(day7, part2)]
pub fn solve_part2(input: &[u32]) -> u32 {
    let target = (input.iter().sum::<u32>() as f32 / input.len() as f32).floor() as u32;
    input.iter().fold(0, |fuel, i| fuel + i.abs_diff(target) * (i.abs_diff(target) + 1) / 2)
}

#[cfg(test)]
static TEST_INPUT: &str = "16,1,2,0,4,2,7,1,2,14";

#[test]
fn test_part1() {
    assert_eq!(37, solve_part1(&input_generator(TEST_INPUT)))
}

#[test]
fn test_part2() {
    assert_eq!(168, solve_part2(&input_generator(TEST_INPUT)))
}
