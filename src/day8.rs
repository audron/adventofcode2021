use std::collections::HashMap;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Display<'a> {
    signals: [&'a str; 10],
    output: [&'a str; 4],
}

bitflags::bitflags! {
    pub struct S: u32 {
        const LOWER_RIGHT = 0b00000001;
        const UPPER_RIGHT = 0b00000010;
        const LOWER_LEFT  = 0b00000100;
        const UPPER_LEFT  = 0b00001000;
        const BOTTOM      = 0b00010000;
        const MIDDLE      = 0b00100000;
        const TOP         = 0b01000000;
    }
}

impl<'a> Display<'a> {
    // top:       2 3 4 5 7 8 9
    // bottom:    2 3   5   8
    // middle:    2 3 4 5   8 9
    // l_left:    2         8
    // u_left:        4 5   8 9
    // l_right: 1 2 3 4   7 8 9
    // u_right: 1   3 4 5 7 8 9
    pub fn map(&self) -> Option<u32> {
        let mut map: HashMap<char, S> = HashMap::new();

        let one = self.signals.iter().find(|s| s.len() == 2).unwrap();
        let four = self.signals.iter().find(|s| s.len() == 4).unwrap();
        let seven = self.signals.iter().find(|s| s.len() == 3).unwrap();
        let eight = self.signals.iter().find(|s| s.len() == 7).unwrap();
        let nine = self
            .signals
            .iter()
            .filter(|s| s.len() == 6)
            .find(|s| four.chars().all(|c| s.contains(c)))?;
        let six = self
            .signals
            .iter()
            .filter(|s| s.len() == 6)
            .find(|s| !one.chars().all(|c| s.contains(c)))?;
        let zero = self
            .signals
            .iter()
            .find(|s| s.len() == 6 && *s != six && *s != nine)?;

        map.insert(seven.chars().find(|c| !one.contains(*c))?, S::TOP);
        map.insert(eight.chars().find(|c| !nine.contains(*c))?, S::LOWER_LEFT);
        let u_right = eight.chars().find(|c| !six.contains(*c))?;
        map.insert(u_right, S::UPPER_RIGHT);
        map.insert(one.chars().find(|c| *c != u_right)?, S::LOWER_RIGHT);
        map.insert(eight.chars().find(|c| !zero.contains(*c))?, S::MIDDLE);
        map.insert(four.chars().find(|c| !map.contains_key(c))?, S::UPPER_LEFT);
        map.insert(('a'..'h').find(|c| !map.contains_key(c))?, S::BOTTOM);

        let zero = S::all() ^ S::MIDDLE;
        let one = S::LOWER_RIGHT | S::UPPER_RIGHT;
        let two = S::MIDDLE | S::BOTTOM | S::LOWER_LEFT | S::UPPER_RIGHT | S::TOP;
        let three = S::MIDDLE | S::BOTTOM | S::LOWER_RIGHT | S::UPPER_RIGHT | S::TOP;
        let four = S::MIDDLE | S::LOWER_RIGHT | S::UPPER_LEFT | S::UPPER_RIGHT;
        let five = S::MIDDLE | S::BOTTOM | S::LOWER_RIGHT | S::UPPER_LEFT | S::TOP;
        let six = S::all() ^ S::UPPER_RIGHT;
        let seven = S::TOP | S::UPPER_RIGHT | S::LOWER_RIGHT;
        let eight = S::all();
        let nine = S::all() ^ S::LOWER_LEFT;

        Some(
            self.output
                .iter()
                .map(|output| {
                    let r = output
                        .chars()
                        .fold(S::empty(), |seg, c| seg | *map.get(&c).unwrap());

                    if r == zero {
                        '0'
                    } else if r == one {
                        '1'
                    } else if r == two {
                        '2'
                    } else if r == three {
                        '3'
                    } else if r == four {
                        '4'
                    } else if r == five {
                        '5'
                    } else if r == six {
                        '6'
                    } else if r == seven {
                        '7'
                    } else if r == eight {
                        '8'
                    } else if r == nine {
                        '9'
                    } else {
                        panic!("aaaa")
                    }
                })
                .collect::<String>()
                .parse()
                .unwrap(),
        )
    }
}

// #[aoc_generator(day8)]
pub fn input_generator<'a>(input: &'a str) -> Vec<Display<'a>> {
    input
        .lines()
        .map(|line| {
            let mut split = line.split('|');
            let signals = split.next().unwrap();
            let output = split.next().unwrap();

            Display {
                signals: signals
                    .split_ascii_whitespace()
                    .collect::<Vec<_>>()
                    .try_into()
                    .unwrap(),
                output: output
                    .split_ascii_whitespace()
                    .collect::<Vec<_>>()
                    .try_into()
                    .unwrap(),
            }
        })
        .collect()
}

// #[aoc(day8, part1)]
// pub fn solve_part1(input: &str) -> u32 {
//     let input = input_generator(input);

//     input
//         .iter()
//         .map(|display| {
//             display
//                 .output
//                 .iter()
//                 .map(|signal| match digit(signal) {
//                     -1 => 0,
//                     _ => 1,
//                 })
//                 .sum::<u32>()
//         })
//         .sum()
// }

#[aoc(day8, part2)]
pub fn solve_part2(input: &str) -> u32 {
    let input = input_generator(input);

    input.iter().filter_map(|display| display.map()).sum()
}

#[cfg(test)]
static TEST_INPUT: &str =
    "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce
";

#[test]
fn test_part1() {
    // assert_eq!(26, solve_part1(TEST_INPUT))
}

#[test]
fn test_part2() {
    assert_eq!(
        5353,
        solve_part2(
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"
        )
    );
    assert_eq!(8394, solve_part2("be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe"));
    assert_eq!(9781, solve_part2("edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc    "));
    assert_eq!(1197, solve_part2("fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg         "));
    assert_eq!(9361, solve_part2("fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb   "));
    assert_eq!(4873, solve_part2("aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea   "));
    assert_eq!(8418, solve_part2("fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb  "));
    assert_eq!(4548, solve_part2("dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe  "));
    assert_eq!(1625, solve_part2("bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef    "));
    assert_eq!(8717, solve_part2("egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb       "));
    assert_eq!(4315, solve_part2("gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce      "));
    assert_eq!(61229, solve_part2(TEST_INPUT));
}
