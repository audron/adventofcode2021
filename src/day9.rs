#[aoc_generator(day9)]
pub fn input_generator(input: &str) -> Vec<Vec<u32>> {
    input
        .lines()
        .map(|line| line.chars().map(|c| c.to_digit(10).unwrap()).collect())
        .collect()
}

fn find_lowest_locations(input: &[Vec<u32>]) -> Vec<(usize, usize)> {
    let mut lowest = Vec::new();

    let y_max = input.len();
    let x_max = input[0].len();

    for y in 0..y_max {
        for x in 0..x_max {
            // match (x, y) {
            //     (0, 0) => (),
            //     (_, 0) => (),
            //     (x_max, 0) => (),
            //     (0, 0) => (),
            //     (0, 0) => (),
            // }

            let mut neighbours = Vec::new();
            if x > 0 {
                neighbours.push(input[y][x - 1]);
            }

            if x < x_max - 1 {
                neighbours.push(input[y][x + 1]);
            }

            if y > 0 {
                neighbours.push(input[y - 1][x]);
            }

            if y < y_max - 1 {
                neighbours.push(input[y + 1][x]);
            }

            if neighbours.iter().all(|i| input[y][x] < *i) {
                lowest.push((x, y));
            }
        }
    }

    return lowest;
}

fn scan_x(points: &mut Vec<(usize, usize)>, input: &[Vec<u32>], point: (usize, usize)) {
    let x_max = input[0].len();

    let x = point.0;
    let y = point.1;
    for x in x..x_max {
        if input[y][x] == 9 {
            break;
        } else if !points.contains(&(x, y)) {
            points.push((x, y));
            scan_y(points, input, (x, y));
        }
    }

    for x in (0..x).rev() {
        if input[y][x] == 9 {
            break;
        } else if !points.contains(&(x, y)) {
            points.push((x, y));
            scan_y(points, input, (x, y));
        }
    }
}

fn scan_y(points: &mut Vec<(usize, usize)>, input: &[Vec<u32>], point: (usize, usize)) {
    let y_max = input.len();
    let x = point.0;
    let y = point.1;

    for y in y..y_max {
        if input[y][x] == 9 {
            break;
        } else if !points.contains(&(x, y)) {
            points.push((x, y));
            scan_x(points, input, (x, y));
        }
    }

    for y in (0..y).rev() {
        if input[y][x] == 9 {
            break;
        } else if !points.contains(&(x, y)) {
            points.push((x, y));
            scan_x(points, input, (x, y));
        }
    }
}

pub fn find_basin_size(input: &[Vec<u32>], lowest: (usize, usize)) -> u32 {
    let mut points: Vec<(usize, usize)> = vec![lowest];

    scan_y(&mut points, input, lowest);
    scan_x(&mut points, input, lowest);

    points.len() as u32
}

#[aoc(day9, part1)]
pub fn solve_part1(input: &[Vec<u32>]) -> u32 {
    let lowest = find_lowest_locations(input);
    lowest.iter().map(|(x, y)| input[*y][*x] + 1).sum()
}

#[aoc(day9, part2)]
pub fn solve_part2(input: &[Vec<u32>]) -> u32 {
    let lowest = find_lowest_locations(input);
    let mut basins: Vec<u32> = lowest
        .iter()
        .map(|lowest| find_basin_size(input, *lowest))
        .collect();
    basins.sort();
    basins.reverse();

    basins[0] * basins[1] * basins[2]
}

#[cfg(test)]
static TEST_INPUT: &str = "2199943210
3987894921
9856789892
8767896789
9899965678
";

#[test]
fn test_find_lowest() {
    assert_eq!(
        vec![(1, 0), (9, 0), (2, 2), (6, 4)],
        find_lowest_locations(&input_generator(TEST_INPUT))
    )
}

#[test]
fn test_basin() {
    assert_eq!(3, find_basin_size(&input_generator(TEST_INPUT), (0, 0)));
    assert_eq!(9, find_basin_size(&input_generator(TEST_INPUT), (9, 0)));
    assert_eq!(14, find_basin_size(&input_generator(TEST_INPUT), (2, 2)));
    assert_eq!(9, find_basin_size(&input_generator(TEST_INPUT), (6, 4)));
}

#[test]
fn test_part2() {
    assert_eq!(1134, solve_part2(&input_generator(TEST_INPUT)));
}
