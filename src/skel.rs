#[aoc_generator(day1)]
pub fn input_generator(input: &str) -> Vec<> {}

#[aoc(day1, part1)]
pub fn solve_part1(input: &[]) -> u32 {}

#[aoc(day1, part2)]
pub fn solve_part2(input: &[]) -> u32 {}
